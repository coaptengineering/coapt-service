using System.IO;
using MongoDB.Driver.GridFS;
using MongoDB.Driver;
using System.Threading.Tasks;
using System.Collections.Generic;
using CoaptService;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using System.Linq;
using CoapService.models;

public class MongoDataAccess : IDataAccess
{

    private MongoClient mongoDB { get; set; }

    public MongoDataAccess()
    {
        var mongoClientSettings = new MongoClientSettings
        {
            Credential = MongoCredential.CreateCredential("coapt", "coapt", "cnaqvsXt8mDV66pd"),
            Server = new MongoServerAddress("199.192.23.210", 27017)
        };
        mongoDB = new MongoClient(mongoClientSettings);

    }

    public async Task<byte[]> GetFirmware(float version)
    {
        var database = mongoDB.GetDatabase("coapt");

        var updatesCollection = database.GetCollection<FirmwareVersion>("updates");
        var firmwareVersion = await updatesCollection.Find(_ => true).SortByDescending(x => x.Version).FirstOrDefaultAsync();
        var bucket = new GridFSBucket(database);
        var stream = new MemoryStream();
        //await bucket.DownloadToStreamByNameAsync(fileName, stream);
        await bucket.DownloadToStreamAsync(firmwareVersion.FileId, stream);
        return stream.ToArray();
    }

    public async Task<CalQal> GetCalQualityMessages(int id)
    {
        var database = mongoDB.GetDatabase("coapt");
        var calqualCollection = database.GetCollection<CalQal>("calQualCopy");
        var message = await calqualCollection.Find(x => x.Id == id).SingleAsync();
        return message;
    }

    public async Task<CableResponse> GetCable(int id)
    {
        var database = mongoDB.GetDatabase("coapt");

        var cableCollection = database.GetCollection<Cable>("cables");
        var profileCollection = database.GetCollection<Profile>("profiles");
        var deviceCollection = database.GetCollection<Device>("devices");
        var motionCollection = database.GetCollection<Motion>("motions");

        var cable = await cableCollection.Find(x => x.Id == id).SingleAsync();

        var profileFilter = Builders<Profile>.Filter.In(x => x.Id, cable.Profiles);
        var profiles = await profileCollection.Find(profileFilter).ToListAsync();

        var cableResponse = new CableResponse()
        {
            Id = cable.Id,
            Name = cable.Name,
            NameDesc = cable.Name,
            Profiles = new List<ProfileResponse>(),
            DefaultProfile = cable.DefaultProfile
        };

        foreach (var profile in profiles)
        {


            var deviceFilter = Builders<Device>.Filter.In(x => x.Id, profile.Devices);
            var devices = await deviceCollection.Find(deviceFilter).ToListAsync();
            var profileResponse = new ProfileResponse()
            {
                Id = profile.Id,
                Active = profile.Active,
                Name = profile.Name,
                Devices = new List<DeviceResponse>()
            };
            cableResponse.Profiles.Add(profileResponse);

            foreach (var device in devices)
            {
                device.Motions.OrderBy(m => m.Order);
                var _motions = new List<Motion>();
                foreach (var _motion in device.Motions)
                {
                    var dbMotion = motionCollection.FindSync(m => m.Id == _motion.Id);
                    _motions.Add(dbMotion.FirstOrDefault());
                }

                //var motionFilter = Builders<Motion>.Filter.In(x => x.Id, device.Motions.Select(n => n.Id));
                // var motions = await motionCollection.Find(motionFilter).ToListAsync();

                var deviceResponse = new DeviceResponse()
                {
                    Id = device.Id,
                    Name = device.Name,
                    Active = device.Active,
                    Type = device.Type,
                    Motions = new List<MotionResponse>(),
                    Handedness = device.Handedness
                };

                foreach (var motion in _motions)
                {
                    deviceResponse.Motions.Add(new MotionResponse()
                    {
                        Id = motion.Id,
                        FullName = motion.FullName,
                        ShortName = motion.ShortName,
                        UiGroup = motion.UiGroup,
                        CalibrationAction = motion.CalibrationAction,
                        CalPrefix = motion.CalPrefix,
                        CalSuffix = motion.CalSuffix
                    });
                }

                profileResponse.Devices.Add(deviceResponse);
            }
        }
        return cableResponse;
    }

    public async Task<List<Cable>> GetCables()
    {
        var database = mongoDB.GetDatabase("coapt");
        var cableCollection = database.GetCollection<Cable>("cables");
        return await cableCollection.Find(_ => true).ToListAsync();
    }

    public async Task<float> GetLatestFirmwareVersion()
    {
        var db = mongoDB.GetDatabase("coapt");
        var updatesCollection = db.GetCollection<FirmwareVersion>("updates");
        //var test = await updatesCollection.Find(_ => true).ToListAsync();
        var result = await updatesCollection.Find(_ => true).SortByDescending(x => x.Version).FirstOrDefaultAsync();
        return result.Version;
    }
}
