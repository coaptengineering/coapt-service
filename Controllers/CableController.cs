using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using CoaptService;

namespace CoaptService.Controllers
{
  [Route("api/device")]
  [ApiController]

  public class CableController : ControllerBase
  {
    private readonly IDataAccess _dataAccessClient;

    public CableController( IDataAccess dataAccessClient) {
      _dataAccessClient = dataAccessClient;
    }

    [HttpGet]
    [Route("cable/{id}")]
    [ProducesResponseType(typeof(List<CableResponse>), 200)]
    [ProducesResponseType(typeof(BadRequestResult), 200)]
    public async Task<ActionResult> Cables(int id)
    {
      try 
      {
        var cable = await _dataAccessClient.GetCable(id);
        return Ok(cable);
      }
      catch(Exception ex) {
        return BadRequest(ex.Message);
      }
    }

    [HttpGet]
    [Route("cables")]
    [ProducesResponseType(typeof(List<Cable>), 200)]
    public async Task<ActionResult> Cables() {
      var cables =  await _dataAccessClient.GetCables();
      return Ok(cables);
    }

  }
  
}
