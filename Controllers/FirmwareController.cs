﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace CoaptService.Controllers
{
    [Route("api/firmware")]
    [ApiController]
    public class FirmwareController : ControllerBase
    {
        private readonly IDataAccess _dataAccessClient;
        public FirmwareController(IDataAccess dataAccessClient) {
            _dataAccessClient = dataAccessClient;
        }
        // GET api/values
        [HttpGet]
        [Route("{version}")]

        public async Task<ActionResult> Get(float version)
        {
            var data =  await _dataAccessClient.GetFirmware(version);
            var result = new FileContentResult(data, "application/octet-stream");
            return result;
            
        }

        [HttpGet]
        [Route("latest")]
        public async Task<ActionResult> GetLatestFirmwareVersion()
        {
            var result = await _dataAccessClient.GetLatestFirmwareVersion();
            return Ok(result);
        }
    }
}
