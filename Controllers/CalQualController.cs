using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.IO;

namespace CoaptService.Controllers
{
    [Route("api/calqual")]
    [ApiController]
    public class CalQualController : ControllerBase
    {
        private readonly IDataAccess _dataAccessClient;
        public CalQualController(IDataAccess dataAccessClient) {
            _dataAccessClient = dataAccessClient;
        }
        // GET api/values
        [HttpGet]
        [Route("{id}")]
        public async Task<ActionResult> Get(int id)
        {
            var data =  await _dataAccessClient.GetCalQualityMessages(id);
            return Ok(data);
            
        }
    }
}
