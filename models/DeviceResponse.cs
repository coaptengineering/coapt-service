using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace CoaptService {
  public class DeviceResponse
  {
    public int Id {get; set;}
    public string Name {get; set;}
    public bool Active {get; set;}
    public string Type {get; set;}
    public List<MotionResponse> Motions {get; set;} // tbd change to motion
    public bool Handedness {get; set;}
  }
}
