﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace CoapService.models
{
    public class FirmwareVersion
    {
        [BsonId]
        public ObjectId Id { get; set; }
        [BsonElement("file")]
        public ObjectId FileId { get; set; }
        [BsonElement("version")]

        public float Version { get; set; }


    }
}
