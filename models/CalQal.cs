using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CoaptService {
  public class CalQal{
    [BsonElement("id")]
    public int Id {get; set;}

    [BsonElement("enum")]
    public string Enum {get; set;}

    [BsonElement("messages")]
    public List<MessageResponse> Messages {get; set;} 
  }

  [BsonIgnoreExtraElements]
  public class MessageResponse {
    [BsonElement("id")]
    public int Id {get; set;}

    [BsonElement("message")]
    public string Message {get; set;}

    [BsonElement("tip")]
    public string Tip {get; set;}
  }
}