namespace CoaptService {
  public class MotionResponse
  {
    public int Id {get; set;}
    public string FullName {get; set;}
    public string ShortName {get; set;}
    public string UiGroup {get; set;}
    public string CalibrationAction {get;set;}
    public string CalPrefix {get; set;}
    public string CalSuffix {get; set;}
  }
}
