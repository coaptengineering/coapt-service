using System.Collections.Generic;

namespace CoaptService {
  public class ProfileResponse
  {
    public int Id {get; set;}
    public string Name {get; set;}
    public bool Active {get; set;}
    public List<DeviceResponse> Devices {get; set;}
  }
}
