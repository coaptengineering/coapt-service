using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CoaptService
{
  public class Cable
  {
    public int Id { get; set; }

    [BsonElement("name")]
    public string Name { get; set; }
    [BsonElement("nameDesc")]
    public string NameDesc { get; set; }

    [BsonElement("active")]
    public bool Active { get; set; }

    [BsonElement("profiles")]
    public IEnumerable<int> Profiles { get; set; }

    [BsonElement("defaultProfile")]
    public int DefaultProfile { get; set; }

  }
}