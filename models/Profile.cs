using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace CoaptService {
  public class Profile
  {
    public int Id {get; set;}

    [BsonElement("name")]
    public string Name {get; set;}

    [BsonElement("active")]
    public bool Active {get; set;}

    [BsonElement("devices")]
    public List<int> Devices {get; set;}

  }
}
