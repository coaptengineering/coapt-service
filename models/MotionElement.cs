using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CoaptService
{
  [BsonIgnoreExtraElements]
  public class MotionElement
  {
    [BsonId]
    public int internalId { get; set; }

    [BsonElement("id")]
    public int Id { get; set; }

    [BsonElement("order")]
    public int Order { get; set; }
  }

}