using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace CoaptService
{
  public class Device
  {
    public int Id { get; set; }

    [BsonElement("name")]
    public string Name { get; set; }

    [BsonElement("active")]
    public bool Active { get; set; }

    [BsonElement("type")]
    public string Type { get; set; }

    [BsonElement("motions")]
    public IEnumerable<MotionElement> Motions { get; set; }

    [BsonElement("flipable")]
    public IEnumerable<int> Flipable { get; set; }

    [BsonElement("handedness")]
    public bool Handedness { get; set; }

    [BsonElement("userGuideId")]
    public string UserGuideId { get; set; }
  }
}
