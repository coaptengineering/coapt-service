using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace CoaptService {
  public class Motion
  {
    public int Id {get; set;}

    [BsonElement("fullName")]
    public string FullName {get; set;}

    [BsonElement("shortName")]
    public string ShortName {get; set;}

    [BsonElement("calibrationAction")]
    public string CalibrationAction {get; set;}

    [BsonElement("uiGroup")]
    public string UiGroup {get; set;}

    [BsonElement("calPrefix")]
    public string CalPrefix {get; set;}

    [BsonElement("calSuffix")]
    public string CalSuffix {get; set;}
  }
}
