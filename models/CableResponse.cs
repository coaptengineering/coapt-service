using System.Collections.Generic;

namespace CoaptService
{
  public class CableResponse
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public List<ProfileResponse> Profiles { get; set; }
    public int DefaultProfile { get; set; }
    public string NameDesc { get; set; }
  }
}