using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CoaptService;

public interface IDataAccess {
      Task<byte[]> GetFirmware(float version);
      Task<float> GetLatestFirmwareVersion();
      Task<CableResponse> GetCable(int id);
      Task<List<Cable>> GetCables();
      Task<CalQal> GetCalQualityMessages(int id);
}